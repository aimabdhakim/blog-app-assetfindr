package models

import (
	"blog-app-assetfindr/config"
	"time"

	"github.com/lib/pq"
	"gorm.io/gorm"
)

var db *gorm.DB

const postTableName string = "post"

type Post struct {
	ID        uint           `gorm:"primarykey"`
	Title     string         `json:"title"`
	Content   string         `json:"content"`
	Tags      pq.StringArray `gorm:"type:text[]" json:"tags"`
	TagIDs    pq.Int64Array  `gorm:"type:integer[]" json:"tagids"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt
}

func init() {
	config.Connect()
	db = config.GetDB()
}

func GetAllPost() ([]Post, error) {
	var posts []Post
	err := db.Table(postTableName).Find(&posts).Error

	return posts, err
}

func (p *Post) GetPost() error {
	err := db.Table(postTableName).Where("id = ?", p.ID).First(&p).Error

	return err
}

func (p *Post) CreatePost() error {
	err := db.Table(postTableName).Create(&p).Error

	return err
}

func (p *Post) UpdatePost() error {
	err := db.Table(postTableName).Where("id = ?", p.ID).
		Updates(Post{Title: p.Title, Content: p.Content, Tags: p.Tags}).Error

	return err
}

func (p *Post) DeletePost() error {
	err := db.Table(postTableName).Where("id = ?", p.ID).Delete(&p).Error

	return err
}
