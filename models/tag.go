package models

import (
	"strings"
	"time"

	"github.com/lib/pq"
	"gorm.io/gorm"
)

const tagTableName string = "tag"

type Tag struct {
	ID        uint   `gorm:"primarykey"`
	Label     string `json:"label"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt
}

func (t *Tag) CreateTag() error {
	err := db.Table(tagTableName).Create(&t).Error

	return err
}

func GetTagByLabel(label string) (Tag, error) {
	var t Tag
	err := db.Table(tagTableName).Where("label = ?", label).First(&t).Error

	return t, err
}

func CheckAndCreate(tags pq.StringArray) []int64 {
	var tagIDs []int64
	var sliceTags []string
	sliceTags = tags

	for _, v := range sliceTags {
		var tagID int
		stringLabel := strings.ToLower(v)
		tag, err := GetTagByLabel(stringLabel)
		if err == gorm.ErrRecordNotFound {
			newTag := Tag{
				Label: stringLabel,
			}
			newTag.CreateTag()
			tagID = int(newTag.ID)
		} else {
			tagID = int(tag.ID)
		}
		tagIDs = append(tagIDs, int64(tagID))
	}

	return tagIDs
}
