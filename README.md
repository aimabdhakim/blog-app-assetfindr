# blog-app-assetfindr



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/aimabdhakim/blog-app-assetfindr.git
git branch -M main
git push -uf origin main
```

## Goals
The goals of this code is to meet the requirement of technical test that was given from AssetFindr

# Instruction
- You need to change host, port, user, db, password depend on your database in config/config.go files
- SQL and Postman files are in "otherfiles" folder, you can execute it separately
- All done, you can do "go run main.go" in your directory files.