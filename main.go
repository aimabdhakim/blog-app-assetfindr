package main

import (
	"blog-app-assetfindr/controller"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "hello world"})
	})

	router := r.Group("/api")
	postRouter := router.Group("/posts")
	postRouter.GET("", controller.GetAllPost)
	postRouter.GET("/:id", controller.GetPost)
	postRouter.POST("", controller.CreatePost)
	postRouter.PUT("/:id", controller.UpdatePost)
	postRouter.DELETE("/:id", controller.DeletePost)

	r.Run(":8000")
}
