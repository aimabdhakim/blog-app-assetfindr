package controller

import (
	"blog-app-assetfindr/models"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/lib/pq"
)

func GetAllPost(ctx *gin.Context) {
	posts, err := models.GetAllPost()
	if err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	ctx.JSON(http.StatusOK, posts)
}

func GetPost(ctx *gin.Context) {
	var err error
	strID := ctx.Param("id")
	if strID == "" {
		err = errors.New("id cannot be empty")
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	id, err := strconv.Atoi(strID)
	if err != nil {
		err = errors.New("id is not int")
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	p := &models.Post{ID: uint(id)}

	err = p.GetPost()
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx.JSON(http.StatusOK, p)
}

func CreatePost(ctx *gin.Context) {
	p := &models.Post{}
	err := ctx.ShouldBindJSON(&p)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	tagIDs := models.CheckAndCreate(p.Tags)
	p.TagIDs = pq.Int64Array(tagIDs)

	err = p.CreatePost()
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx.JSON(http.StatusOK, p)
}

func UpdatePost(ctx *gin.Context) {
	var err error
	updateP := &models.Post{}
	err = ctx.ShouldBindJSON(&updateP)
	if err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	strID := ctx.Param("id")
	if strID == "" {
		err = errors.New("id cannot be empty")
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	id, err := strconv.Atoi(strID)
	if err != nil {
		err = errors.New("id is not int")
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	p := models.Post{ID: uint(id)}
	err = p.GetPost()
	if err != nil {
		err = errors.New("invalid post id")
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	if updateP.Content != "" {
		p.Content = updateP.Content
	}
	if updateP.Title != "" {
		p.Title = updateP.Title
	}
	if len(updateP.Tags) != 0 {
		p.Tags = updateP.Tags
		tagIDs := models.CheckAndCreate(p.Tags)
		p.TagIDs = pq.Int64Array(tagIDs)
	}

	err = p.UpdatePost()
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx.JSON(http.StatusOK, p)
}

func DeletePost(ctx *gin.Context) {
	var err error
	strID := ctx.Param("id")
	if strID == "" {
		err = errors.New("id cannot be empty")
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	id, err := strconv.Atoi(strID)
	if err != nil {
		err = errors.New("id is not int")
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	p := models.Post{ID: uint(id)}
	err = p.GetPost()
	if err != nil {
		err = errors.New("invalid post id")
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	err = p.DeletePost()
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}
	ctx.JSON(http.StatusOK, p)
}
